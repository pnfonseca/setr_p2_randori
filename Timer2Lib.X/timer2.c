/*
 * timer2.c
 *
 * Módulo para controlo do Timer2
 */

#ifndef _UNITTESTING_
#include <xc.h>
#else
#include "mem_map.h"
#endif

#include "timer2.h"




int8_t set_timer2_freq(uint16_t Prescaler, uint32_t fout)
{
    int8_t result;
    int32_t contagem;
    

    contagem = PBCLKfreq/(Prescaler*fout)-1;
    if (contagem <1 || contagem> 0xFFFF)
    {
        result = -1;
    }
    else
    {
        result= config_timer2(Prescaler, contagem );
    }


    return result;

}


int8_t config_timer2(uint16_t Prescaler, uint16_t ValPR2)
{
	int8_t retval = -1;
	
    /* Guarantee that Timer 2 is not running */
    T2CONbits.ON = 0;

    /* Reset the timer counter to zero */
    TMR2 = 0;

    /* Configuration of Timer2 hardware */
    T2CONbits.TGATE = 0;
    T2CONbits.TCS = 0;

    /* Set TCKPS and PR2 */
	int8_t k_presc = -1;
	switch(Prescaler) {
		case 1:
			k_presc = 0;
			break;
		case 2:
			k_presc = 1;
			break;
		case 4:
			k_presc = 2;
			break;
		case 8:
			k_presc = 3;
			break;
		case 16:
			k_presc = 4;
			break;
		case 32:
			k_presc = 5;
			break;
		case 64:
			k_presc = 6;
			break;
		case 256:
			k_presc = 7;
			break;
		default:
			break;
	}
	if(k_presc != -1) {
		T2CONbits.TCKPS = k_presc;
		PR2 = ValPR2;
		retval = 0;
	}
	
    return retval;

}

void Timer2control(TimerStates_t T2state)
{
	if(T2state == TOn) {
		T2CONbits.ON = 1;
	}
	else {
		T2CONbits.ON = 0;
	}
	return;
}
