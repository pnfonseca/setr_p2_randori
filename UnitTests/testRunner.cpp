/*
 * testRunner.cpp
 *
 *  Created on: Dec 9, 2012
 *      Author: pf
 */


#include <CppUTest/TestHarness.h>

extern "C" {
#include "timer2.h"
#include "mem_map.h"
}



TEST_GROUP(TimerTest)
{
	void setup()
	{

	}

	void teardown()
	{

	}
};


TEST(TimerTest,SetTimerOn){

	/* Clear all bits in T2CON */
	T2CON = 0x0000;

	Timer2control(TOn);

	CHECK_EQUAL(0x8000, T2CON);

}

TEST(TimerTest,ResetTimer){

	/* Set TON bit */
	T2CONbits.ON = 1;

	Timer2control(TOff);

	CHECK_EQUAL(0, T2CONbits.ON);

}

TEST(TimerTest, ValidPrescaler) {
    int8_t retval = config_timer2(4, 1000);
    CHECK_EQUAL(0, retval);
    retval = config_timer2(3, 1000);
    CHECK_EQUAL(-1, retval);
    
    uint16_t prescalers[8] = {1, 2, 4, 8, 16, 32, 64, 256};
    int8_t i;
    for(i=0; i<8; i++) {
        retval = config_timer2(prescalers[i], 1000);
        CHECK_EQUAL(0, retval);
    }
    
 
}

TEST(TimerTest, ValidPrescaler_Fout)
{
    int8_t retval;
    
    retval = set_timer2_freq(4,1000);
    CHECK_EQUAL(0, retval);
    
    retval = set_timer2_freq(4, 10);
    CHECK_EQUAL(-1, retval);
 
            
}